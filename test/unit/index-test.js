/* global it, describe, beforeEach */
/* eslint prefer-arrow-callback: "off" */

const chai = require('chai');
const confirmationHelper = require('../../src/index');
const interceptIO = require('@sector12/intercept-io');
const stdin = require('mock-stdin').stdin();

describe('index-test.js', function () {
    // Reset stdin before each test
    beforeEach(function () {
        stdin.reset();
    });

    describe('getUserConfirmation', function () {
        it('should return true when default value is true and no input is given', function () {
            // Ask the user to confirm
            let promise;
            interceptIO.interceptStdoutAndStderr(function () {
                promise = confirmationHelper.getUserConfirmation('Are you sure?', true).then(
                    function (result) {
                        chai.assert.isTrue(result);
                    });
            });

            // Send the response value to stdin after this iteration of event loop finishes.
            // Be sure to intercept io again, as this will execute on a different stack.
            process.nextTick(function () {
                interceptIO.interceptStdoutAndStderr(function () {
                    stdin.send('\n');
                    stdin.send(null);
                });
            });

            return promise;
        });

        it('should return false when default value is false and no input is given', function () {
            // Ask the user to confirm
            let promise;
            interceptIO.interceptStdoutAndStderr(function () {
                promise = confirmationHelper.getUserConfirmation('Are you sure?', false).then(
                    function (result) {
                        chai.assert.isFalse(result);
                    });
            });

            // Send the response value to stdin after this iteration of event loop finishes.
            // Be sure to intercept io again, as this will execute on a different stack.
            process.nextTick(function () {
                interceptIO.interceptStdoutAndStderr(function () {
                    stdin.send('\n');
                    stdin.send(null);
                });
            });

            return promise;
        });

        it('should return false when invalid input is given (even if default value is true)', function () {
            // Ask the user to confirm
            let promise;
            interceptIO.interceptStdoutAndStderr(function () {
                promise = confirmationHelper.getUserConfirmation('Are you sure?', true).then(
                    function (result) {
                        chai.assert.isFalse(result);
                    });
            });

            // Send the response value to stdin after this iteration of event loop finishes.
            // Be sure to intercept io again, as this will execute on a different stack.
            process.nextTick(function () {
                interceptIO.interceptStdoutAndStderr(function () {
                    stdin.send('Invalid input.');
                    stdin.send(null);
                });
            });

            return promise;
        });

        it('should default to false when default value is not provided', function () {
            // Ask the user to confirm
            let promise;
            interceptIO.interceptStdoutAndStderr(function () {
                promise = confirmationHelper.getUserConfirmation('Are you sure?').then(
                    function (result) {
                        chai.assert.isFalse(result);
                    });
            });

            // Send the response value to stdin after this iteration of event loop finishes.
            // Be sure to intercept io again, as this will execute on a different stack.
            process.nextTick(function () {
                interceptIO.interceptStdoutAndStderr(function () {
                    stdin.send('\n');
                    stdin.send(null);
                });
            });

            return promise;
        });
    });
});
