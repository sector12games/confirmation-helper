const jsdocConfig = require('@sector12/jsdoc-config');
const gulp = require('gulp');
const gulpTasks = require('@sector12/gulp-tasks');
const runSequence = require('run-sequence');

gulp.task('doc', (cb) => {
    gulpTasks.runDoc(cb, gulpTasks.nodeProjectStandardDocFiles, jsdocConfig);
});

gulp.task('lint', () => {
    return gulpTasks.runLint(gulpTasks.nodeProjectStandardLintFiles);
});
gulp.task('lint:fix', () => {
    return gulpTasks.runLint(gulpTasks.nodeProjectStandardLintFiles, true);
});

gulp.task('test', (cb) => {
    runSequence('test:unit', cb);
});
gulp.task('test:unit', (cb) => {
    gulpTasks.runMochaTests('test/unit', cb);
});

gulp.task('default', (cb) => {
    runSequence('lint', 'test', 'doc', cb);
});
