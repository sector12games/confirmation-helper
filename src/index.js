const inquirer = require('inquirer');

/**
 * Asks the user for confirmation before proceeding. Returns a promise that
 * will resolve to true or false.
 *
 * @param {string} message - The confirmation message to display to the user.
 * @param {boolean} [defaultValue=false] - The default value to return when
 *      the user does not specify any input.
 * @returns {Promise} A promise that resolves to true or false.
 */
function getUserConfirmation(message, defaultValue = false) {
    const question = {
        type: 'confirm',
        name: 'isConfirmationGranted',
        message,
        default: defaultValue,
    };

    // Promises are clever. Inquirer.prompt() will return an unresolved promise.
    // .then() will return another unresolved promise that will resolve after the
    // first promise resolves. This means that we'll be returning a flat "true/false"
    // boolean value from this function, after the promise has resolved.
    return inquirer.prompt(question).then((answers) => { return answers.isConfirmationGranted; });
}

module.exports = {
    getUserConfirmation,
};
