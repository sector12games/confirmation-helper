## confirmation-helper

A helper function that wraps inquirer to return a promise that resolves to a
simple true/false.
